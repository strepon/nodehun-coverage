const Nodehun = require("nodehun");
const fsPromises = require("fs").promises;

const dictionariesDir = "dictionaries/";
const textsDir = "texts/";

const getFile = function(fileName) {
    return fsPromises.readFile(fileName);
};

const getFilesInDir = function(dirName) {
    return fsPromises.readdir(dirName);
};

const getDictFiles = function(dictName) {
    const promises = [];
    promises.push(getFile(dictionariesDir + dictName + ".aff"));
    promises.push(getFile(dictionariesDir + dictName + ".dic"));

    return Promise.all(promises);
};

const checkWords = function(words, dict) {
    const obtainedChecks = [];
    for (let w = 0; w < words.length; w++) {
        obtainedChecks.push(dict.spell(words[w]));
    }
    return Promise.all(obtainedChecks);
};

const results = {};
getFilesInDir(dictionariesDir)
    .then(function(allDictFiles) {
        allDictFiles = allDictFiles.filter(file => file.split(".").length > 1 && file.split(".")[1] === "dic");
        const promisesDicts = [];
        for (let d = 0; d < allDictFiles.length; d++) {
            promisesDicts.push(new Promise(function(resolve, reject) {
                const dictName = allDictFiles[d].split(".")[0];
                results[dictName] = {};

                getDictFiles(dictName)
                    .then(function(dictFiles) {
                        const dict = new Nodehun(dictFiles[0], dictFiles[1]);

                        getFilesInDir(textsDir)
                            .then(function(files) {
                                const promisesFiles = [];
                                for (let f = 0; f < files.length; f++) {
                                    promisesFiles.push(new Promise(function(resolve, reject) {
                                        let words;
                                        getFile(textsDir + files[f]).then(function(file) {
                                            words = file.toString();
                                            words = words.replace(/,|"|„|“|'|’|´|‚|‘| -| –|\?|!|:|;|\(|\)|\*|…|•|\||\/|%|\r|\n/gi, " ");
                                            words = words.split(" ");
                                            // split by dot but keep the dot always at the end of word
                                            for (let w = words.length - 1; w >= 0; w--) {
                                                let splitByDot = words[w].split(/(\.)/);
                                                if (splitByDot.length > 1) {
                                                    for (let sw = 0; sw < splitByDot.length; sw++) {
                                                        if (splitByDot[sw] === "." && sw > 0) {
                                                            splitByDot[sw - 1] += ".";
                                                        }
                                                    }
                                                    splitByDot = splitByDot.filter(word => word !== ".");
                                                    words.splice(w, 1, ...splitByDot);
                                                }
                                            }
                                            words = words.filter(word => word !== "");
                                            return words;
                                        })
                                            .then(function(words) {
                                                return checkWords(words, dict);
                                            })
                                            .then(function(checkedWords) {
                                                const incorrectWords = new Map();
                                                let incorrectCount = 0;
                                                for (let w = 0; w < words.length; w++) {
                                                    if (!checkedWords[w]) {
                                                        incorrectCount++;
                                                        const incorrectWord = words[w].replace(/\.$/g, "");
                                                        const newWordCount = incorrectWords.has(incorrectWord) ? incorrectWords.get(incorrectWord) + 1 : 1;
                                                        incorrectWords.set(incorrectWord, newWordCount);
                                                    }
                                                }
                                                results[dictName][files[f]] = (100 * incorrectCount / checkedWords.length).toFixed(2);

                                                const outputIncorrect = process.argv.length > 2 && process.argv[2] === "--output-incorrect";
                                                if (outputIncorrect) {
                                                    const incorrectWordCounts = [];
                                                    for (const [word, count] of incorrectWords) {
                                                        incorrectWordCounts.push({ word: word, count: count });
                                                    }
                                                    incorrectWordCounts.sort((word1, word2) => word1.count > word2.count ? -1 : (word1.count === word2.count ? 0 : 1));

                                                    let wordCountOutput = "";
                                                    for (const incorrectWordCount of incorrectWordCounts) {
                                                        wordCountOutput += `${incorrectWordCount.word}\t${incorrectWordCount.count}\n`;
                                                    }
                                                    return fsPromises.writeFile(`incorrect_words_${files[f]}_${dictName}`, wordCountOutput);
                                                }
                                                return undefined;
                                            })
                                            .then(function() {
                                                resolve(files[f]);
                                            })
                                            .catch(function(error) {
                                                console.log(error);
                                            });
                                    }));
                                }
                                return Promise.all(promisesFiles);
                            })
                            .then(function(files) {
                                resolve(files);
                            });
                    });
            }));
        }
        return Promise.all(promisesDicts);
    })
    .then(function(texts) {
        const dicts = Object.getOwnPropertyNames(results);
        const coveragesForTexts = ["\t" + dicts.join("\t")];
        texts = texts[0];
        for (const text of texts) {
            const coverages = [];
            for (const dict of dicts) {
                coverages.push(results[dict][text]);
            }
            coveragesForTexts.push(text + "\t" + coverages.join("\t"));
        }
        return fsPromises.writeFile("coverage.txt", coveragesForTexts.join("\n"));
    });
