nodehun-coverage
================

This script determines coverage of texts by Hunspell dictionaries.
[Nodehun](https://www.npmjs.com/package/nodehun) is employed for that.

Requirements: Node.js >= 12, Nodehun >= 3

Place pairs (.dic and .aff) of your dictionary files to the `dictionaries` directory,
the texts to be evaluated as plain text files to the `texts` directory and run the script:
`node nodehun_coverage.js`. The percentage of uncovered words for each
combination of dictionary and text is outputted to the `coverage.txt` file.

## Arguments
`--output-incorrect`: outputs a list of incorrect words for each combination of dictionary and text.
